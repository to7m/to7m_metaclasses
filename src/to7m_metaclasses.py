from itertools import islice


class PrintAudit(type):
    """
    A quick tool to audit every method call in a class.
    """

    indent = '    '
    limit = 79 - len(indent)
    _overflow = object()

    def _arg_strs(cls, xargs, kwargs):
        arg_strs = []
        for xarg in xargs:
            arg_strs.append(repr(xarg))
        for key, value in kwargs.items():
            arg_strs.append(f"{key}={repr(value)}")
        return arg_strs

    def _single_line(cls, callable_str, arg_strs):
        lines = [f"{callable_str}({', '.join(arg_strs)})"]
        if arg_strs and len(lines[0]) > cls.limit:
            return cls._overflow
        else:
            return lines

    def _multi_line(cls, callable_str, arg_strs):
        indent = len(callable_str) + 1
        lines = []
        line = [f"{callable_str}({arg_strs[0]}"]
        line_len = len(line[0])
        if line_len + 1 > cls.limit:
            return cls._overflow

        for arg_str in islice(arg_strs, 1, None):
            if line_len + 2 + len(arg_str) + 1 > cls.limit:
                line.append(',')
                lines.append(''.join(line))
                line = [f"{indent}{arg_str}"]
                line_len = len(line[0])
                if line_len + 1 > cls.limit:
                    return cls._overflow
            else:
                line.append(f", {arg_str}")
                line_len += len(line[-1])
        line.append(')')
        lines.append(''.join(line))

        return lines

    def _immediate_break(cls, callable_str, arg_strs):
        indent = cls.indent
        lines = [f"{callable_str}("]
        line = [f"{indent}{arg_strs[0]}"]
        line_len = len(line[0])
        for arg_str in islice(arg_strs, 1, None):
            if line_len + 2 + len(arg_str) + 1 > cls.limit:
                line.append(',')
                lines.append(''.join(line))
                line = [f"{indent}{arg_str}"]
                line_len = len(line[0])
            else:
                line.append(f", {arg_str}")
                line_len += len(line[-1])
        line.append(')')
        lines.append(''.join(line))

        return lines

    def _before_str(cls, callable_str, xargs, kwargs):
        arg_strs = cls._arg_strs(cls, xargs, kwargs)
        lines = cls._single_line(cls, callable_str, arg_strs)
        if lines is cls._overflow:
            lines = cls._multi_line(cls, callable_str, arg_strs)
            if lines is cls._overflow:
                lines = cls._immediate_break(cls, callable_str, arg_strs)
        return f"\n{cls.indent}".join([f"{callable_str}: entering"] + lines)

    def _after_str(cls, callable_str, returned_val):
        return (f"{callable_str}: exiting\n"
                f"{cls.indent}{repr(returned_val)}")

    def _wrap_callable(cls, class_str, callable_name, callable_obj):
        callable_str = f"{class_str}.{callable_name}"

        def wrapped_callable(self, *xargs, **kwargs):
            print(cls._before_str(cls, callable_str, xargs, kwargs))
            returned_val = callable_obj(self, *xargs, **kwargs)
            print(cls._after_str(cls, callable_str, returned_val))
        return wrapped_callable

    def __new__(cls, name, bases, attrs_dict):
        class_str = f"{attrs_dict['__module__']}.{name}()"

        new_attrs_dict = {}
        for key, value in attrs_dict.items():
            if callable(value):
                value = cls._wrap_callable(cls, class_str, key, value)
            new_attrs_dict[key] = value

        return super().__new__(cls, name, bases, new_attrs_dict)

import to7m_context
from to7m_metaclasses import PrintAudit


class TestClass(metaclass=PrintAudit):
    uncallable_attribute_not_intercepted = 7000

    def __init__(self, method_being_run_str):
        self.method_being_run_str = method_being_run_str

    def method(self, a, b, c, d, e, f, g, h, i, j):
        print(self.method_being_run_str)
        return [a ** x for x in range(1, b + 1)]


test_obj = TestClass("the method is currently being run")
test_obj.method(5, 6, "what are these", "tricks?",
                "badger" * 10, [f"list item {i}" for i in range(5)],
                g="position arg", h="running out of test ideas",
                i=range(100), j=type(type(test_obj)))
